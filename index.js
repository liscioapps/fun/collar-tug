var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
require('dotenv').config();

var tug = require('./lib/tug');

tug.verifyEnv();

var app = express()
app.use(morgan('dev'))
app.use(express.static('public'))

app.route('/collartug')
  .get(function (req, res) {
    res.sendStatus(200)
  })
  .post(bodyParser.urlencoded({ extended: true }), function (req, res) {
    if (req.body.token !== tug.VERIFY_TOKEN) {
      return res.sendStatus(401)
    }

    var message = 'https://media.giphy.com/media/WRMq4MMApzBeg/giphy.gif'
    let gif = tug.getGif(req.body.text);

    if (gif) {
      message = gif.url
    }

    // Handle any help requests
    if (req.body.text === 'help') {
      message = "Sorry, I can't offer much help, just here to https://media.giphy.com/media/WRMq4MMApzBeg/giphy.gif"
    }

    res.json({
      response_type: 'in_channel',
      text: message
    })
  })

let web = require('./lib/web')(app);

app.listen(tug.PORT, function (err) {
  if (err) {
    return console.error('Error starting server: ', err)
  }

  console.log('Server successfully started on port %s', tug.PORT)
})
