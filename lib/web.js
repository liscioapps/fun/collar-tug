var path = require('path');
var enforce = require('express-sslify');

var webApp = function Constructor(app) {
    console.log(process.env.NODE_ENV)
    //app.use(enforce.HTTPS());

    this.express = require('express');
    this.hbs = require('handlebars');
    this.expresshbs = require('express-handlebars');
    this.bodyparser = require('body-parser');

    app.engine('handlebars', this.expresshbs(require('./hbs-config')));
    app.set('view engine', 'handlebars');

    app.route('/')
    .get(function (req, res) {
        res.render('home');
    })

    oauth = require('./oauth')(app);
    
}

module.exports = webApp