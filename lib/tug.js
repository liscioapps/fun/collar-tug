let gifs = require('./loaddata')
var VERIFY_TOKEN = process.env.SLACK_VERIFY_TOKEN
var PORT = process.env.PORT
var CLIENT_ID = process.env.CLIENT_ID
var CLIENT_SECRET = process.env.CLIENT_SECRET
var REDIRECT_URI = process.env.REDIRECT_URI

module.exports = {
    verifyEnv: function() {
        
        if (!VERIFY_TOKEN) {
            console.error('SLACK_VERIFY_TOKEN is required')
            process.exit(1)
        }
        
        if (!PORT) {
            console.error('PORT is required')
            process.exit(1)
        }

        if (!CLIENT_ID) {
            console.error('CLIENT_ID is required')
            process.exit(1)
        }

        if (!CLIENT_SECRET) {
            console.error('CLIENT_SECRET is required')
            process.exit(1)
        }

        if (!REDIRECT_URI) {
            console.error('REDIRECT_URI is required')
            process.exit(1)
        }
    },
    getGif: function(text) {
        text = text.toString().toLowerCase();
        let theObj = gifs.find(o => o.id === text);
        return theObj;
    },
    gifs: gifs,
    PORT: PORT,
    VERIFY_TOKEN: VERIFY_TOKEN
}